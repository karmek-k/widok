import socketserver

from src.storage import Storage


class RequestHandler(socketserver.BaseRequestHandler):
    DATA_SIZE = 64 * 1024 * 1024  # 64 KiB

    def __init__(self, request, client_address, server):
        super().__init__(request, client_address, server)

    def handle(self):
        data = self.request.recv(self.DATA_SIZE).decode('utf-8').strip()
        command = data[0]

        if command == '-':
            self.request.sendall(bytes(Storage.retrieve(), 'utf-8'))
        elif command == '+':
            Storage.store(data[1:])
            self.request.sendall(bytes('+', 'utf-8'))
        else:
            self.request.sendall(bytes('#', 'utf-8'))
