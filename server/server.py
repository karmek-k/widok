from os import getenv, unlink
import socketserver

from src.socket import RequestHandler


def main():
    socket_path = getenv('HOME') + '/.widok/widok.sock'

    try:
        with socketserver.UnixStreamServer(socket_path, RequestHandler) as server:
            server.serve_forever()
    except:
        pass
    finally:
        unlink(socket_path)


if __name__ == '__main__':
    main()
